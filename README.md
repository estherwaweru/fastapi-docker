# fastapi-docker
Fastapi sample application with docker configuration

This project uses Tortoise orm,Postgres Database ,FastApi,Docker,Docker Compose and
has a Github actions for CI/CD.
It has endpoints for crud for users.
The project is deployed in heroku [https://dashboard.heroku.com/apps/fastapi-docker-tutorial]
